# Instructions

## The Exercise
Create a simple, 2 endpoint JSON api that fetches data from an external source.

## Acceptance Criteria
* Fetch data from https://hokodo-frontend-interview.netlify.com/data.json
* Endpoint `/books` should return a collection of books
* Endpoint `/books` should be capable of returning data sorted by publication date, ascending and descending
* Endpoint `/books` should be capable of returning data sorted alphabetically by title, ascending and descending
* Endpoint `/authors` should return a collection of authors, each author item should contain a collection of their books

## Requirements
A good solution will
* satisfy the acceptance criteria
* have good test coverage
* use _appropriate_ libraries
* be production ready


# Usage

## Running the tests
```console
pip install -r requirements.pip
pip install -r test-requirements.pip
pytest
```

## Running the application
```console
pip install -r requirements.pip
FLASK_APP=hokodo flask run
```
Then point your browser of choice to `localhost:5000/books`

## Sorting the API outputs
You can use the querystring to sort the API outputs like thus:
* Sorting by title ascending: `localhost:5000/books?sort=title:asc`
* Sorting by title descending: `localhost:5000/books?sort=title:desc`
* Ascending is used by default: `localhost:5000/books?sort=title`


# Next

* package?
* CI
* linter
* mypy
* coverage
* ansible deploy playbook
* docker container
* authentification
* asynchronous backend
* pagination
