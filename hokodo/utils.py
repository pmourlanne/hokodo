from collections import namedtuple

from hokodo.exceptions import OrderingException


Ordering = namedtuple('Ordering', ['key', 'reverse'])


def extract_ordering(sort_string):
    if not sort_string:
        return None

    if ':' in sort_string:
        try:
            sort_key, reverse = sort_string.split(':')
        except ValueError:
            msg = "Could not extract ordering from {}".format(sort_string)
            raise OrderingException(msg)

        reverse = reverse == 'desc'
        return Ordering(sort_key, reverse)

    # reverse defaults to False
    return Ordering(sort_string, False)
