from flask import request

from hokodo import app
from hokodo.backend import get_backend
from hokodo.exceptions import OrderingException
from hokodo.utils import extract_ordering


@app.route('/books')
def books():
    backend = get_backend()
    try:
        order_by = extract_ordering(request.args.get('sort'))
    except OrderingException as e:
        return str(e), 400
    return backend.fetch_books(order_by)


@app.route('/authors')
def authors():
    backend = get_backend()
    return backend.fetch_authors()
