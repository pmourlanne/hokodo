from datetime import datetime

import pytest


@pytest.fixture
def books():
    return [
        # `published` do not have a TZ, add one?
        {
            "title": "Blood Meridian",
            "author": "Cormac McCarthy",
            "published": datetime(1985, 4, 1).isoformat(),
            "pages": 337,
        },
        {
            "title": "The Road",
            "author": "Cormac McCarthy",
            "published": datetime(2006, 9, 26).isoformat(),
            "pages": 287,
        },
        {
            "title": "Catch-22",
            "author": "Joseph Heller",
            "published": datetime(1961, 11, 10).isoformat(),
            "pages": 453,
        },
        {
            "title": "Slaughterhouse-Five",
            "author": "Kurt Vonnegut",
            "published": datetime(1969, 3, 31).isoformat(),
            "pages": 275,
        },
        {
            "title": "Lab Rats",
            "author": "Dan Lyons",
            "published": datetime(2018, 10, 23).isoformat(),
            "pages": 272,
        },
        {
            "title": "Disrupted",
            "author": "Dan Lyons",
            "published": datetime(2016, 4, 5).isoformat(),
            "pages": 272,
        },
    ]


@pytest.fixture
def authors():
    return {
        'Cormac McCarthy': [
            {
                'title': 'Blood Meridian',
                'author': 'Cormac McCarthy',
                'published': '1985-04-01T00:00:00',
                'pages': 337,
            },
            {
                'title': 'The Road',
                'author': 'Cormac McCarthy',
                'published': '2006-09-26T00:00:00',
                'pages': 287,
            },
        ],
        'Joseph Heller': [
            {
                'title': 'Catch-22',
                'author': 'Joseph Heller',
                'published': '1961-11-10T00:00:00',
                'pages': 453,
            },
        ],
        'Kurt Vonnegut': [
            {
                'title': 'Slaughterhouse-Five',
                'author': 'Kurt Vonnegut',
                'published': '1969-03-31T00:00:00',
                'pages': 275,
            },
        ],
        'Dan Lyons': [
            {
                'title': 'Lab Rats',
                'author': 'Dan Lyons',
                'published': '2018-10-23T00:00:00',
                'pages': 272,
            },
            {
                'title': 'Disrupted',
                'author': 'Dan Lyons',
                'published': '2016-04-05T00:00:00',
                'pages': 272,
            },
        ],
    }
