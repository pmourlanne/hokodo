import operator

import pytest

from hokodo.backend import SynchronousBackend
from hokodo.utils import Ordering


@pytest.fixture
def sync_backend():
    return SynchronousBackend()


@pytest.fixture
def mock_return_data(books, requests_mock, sync_backend):
    url = sync_backend.DATA_URL
    requests_mock.get(url, json={'books': books})


def test__fetch_data(books, sync_backend, mock_return_data):
    data = sync_backend._fetch_data()
    # We get the mock data
    assert 'books' in data
    assert len(data['books']) == 6
    assert data['books'][0] == books[0]


def test_fetch_books(books, sync_backend, mock_return_data):
    assert sync_backend.fetch_books() == books


@pytest.mark.parametrize('ordering,key,reverse', [
    (Ordering('title', False), 'title', False),
    (Ordering('title', True), 'title', True),
    (Ordering('published', False), 'published', False),
    (Ordering('published', True), 'published', True),
    # Ordering works for other keys
    (Ordering('pages', False), 'pages', False),
    (Ordering('pages', True), 'pages', True),
    # Ordering is ignored if key is unknown
    (Ordering('nimportequoi', False), None, None),
    (Ordering('nimportequoi', True), None, None),
])
def test_fetch_books_order_by(
        books, sync_backend, key, reverse,
        mock_return_data, ordering):

    if key:
        expected = sorted(books, key=operator.itemgetter(key), reverse=reverse)
    else:
        expected = books
    assert sync_backend.fetch_books(ordering=ordering) == expected


def test_fetch_authors(authors, sync_backend, mock_return_data):
    assert sync_backend.fetch_authors() == authors
