import pytest

from hokodo.exceptions import OrderingException
from hokodo.utils import extract_ordering
from hokodo.utils import Ordering


@pytest.mark.parametrize('sort_string,expected', [
    (None, None),
    # `reverse` is False by default
    ('title', Ordering('title', False)),
    ('title:asc', Ordering('title', False)),
    ('title:desc', Ordering('title', True)),
    # `extract_ordering` does not check keys
    ('nimportequoi', Ordering('nimportequoi', False)),
])
def test_extract_ordering(sort_string, expected):
    assert extract_ordering(sort_string) == expected


@pytest.mark.parametrize('sort_string', [
    'title:asc,author:desc',
])
def test_extract_ordering_raise(sort_string):
    with pytest.raises(OrderingException):
        extract_ordering(sort_string)
