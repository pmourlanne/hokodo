import pytest

from hokodo import app
from hokodo.exceptions import OrderingException


@pytest.fixture
def client():
    return app.test_client()


@pytest.fixture
def mock_backend(mocker, books, authors):
    mock_backend = mocker.patch('hokodo.backend.SynchronousBackend')

    mock_backend.return_value.fetch_books.return_value = books
    mock_backend.return_value.fetch_authors.return_value = authors

    return mock_backend


def test_books_ordering_exception(mocker, client):
    mocker.patch(
        'hokodo.views.extract_ordering',
        side_effect=OrderingException('alo'),
    )

    response = client.get('/books')
    assert response.status_code == 400
    assert response.data == b'alo'


def test_books(mock_backend, client, books):
    response = client.get('/books')
    assert response.status_code == 200
    assert response.json == books


def test_authors(mock_backend, client, authors):
    response = client.get('/authors')
    assert response.status_code == 200
    assert response.json == authors
