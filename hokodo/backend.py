import operator
from collections import defaultdict

import requests


class QueryBackend():
    def fetch_books(self, ordering=None):
        raise NotImplementedError()

    def fetch_authors(self):
        raise NotImplementedError()


class SynchronousBackend(QueryBackend):
    DATA_URL = "https://hokodo-frontend-interview.netlify.com/data.json"

    def _fetch_data(self):
        r = requests.get(self.DATA_URL)
        r.raise_for_status()
        return r.json()

    def fetch_books(self, ordering=None):
        books = self._fetch_data()['books']
        if not ordering:
            return books

        try:
            return sorted(
                books,
                key=operator.itemgetter(ordering.key),
                reverse=ordering.reverse,
            )
        except KeyError:
            # One book did not have the ordering key
            # TODO: log error, raise?
            return books

    def fetch_authors(self):
        books = self._fetch_data()['books']
        authors = defaultdict(list)
        for book in books:
            authors[book['author']].append(book)

        return authors


def get_backend():
    return SynchronousBackend()
